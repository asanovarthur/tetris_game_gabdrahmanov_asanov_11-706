﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter1Task15
{
    class Program
    {
        static void Main()
        {
            int ticket = int.Parse(Console.ReadLine());
            Console.WriteLine(CheckHappyTicketPiter(ticket));
            
        }
        public static bool CheckHappyTicketPiter(int number)
        {
            return (number / 100000 + (number / 1000) % 10 + (number / 10) % 10 == 
                (number / 10000) % 10 + (number / 100) % 10 + number % 10);
        }
    }
}

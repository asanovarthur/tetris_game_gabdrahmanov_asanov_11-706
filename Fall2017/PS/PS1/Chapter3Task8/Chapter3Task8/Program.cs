﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter3Task8
{
    class Program
    {
        static void Main()
        {
            int prenumber = int.Parse(Console.ReadLine());
            int number = int.Parse(Console.ReadLine());
            int nextnumber = int.Parse(Console.ReadLine());
            Console.WriteLine(CheckSaw(prenumber, number, nextnumber));
        }
        public static bool CheckSaw(int prenumber, int number, int nextnumber)
        {
            while (nextnumber != 0)
            {
                if ((prenumber - number) * (nextnumber - number) <= 0) break;
                prenumber = number;
                number = nextnumber;
                nextnumber = int.Parse(Console.ReadLine());
            }
            if (nextnumber == 0) nextnumber = prenumber;
            return ((prenumber - number) * (nextnumber - number) > 0);
        }
    }
}

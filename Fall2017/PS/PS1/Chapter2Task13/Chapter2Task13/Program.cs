﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter2Task13
{
    class Program
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            int k = int.Parse(Console.ReadLine());
            Console.WriteLine(GetSum(n, k));
        }
        public static int GetSum(int n, int k)
        {
            int sum = 0;
            while (n > 0)
            {
                sum = sum + n % k;
                n /= k;
            }
            return sum;
        
        }
    }
}

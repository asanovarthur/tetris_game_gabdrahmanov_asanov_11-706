﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter4Task9
{
    class Program
    {
        static void Main()
        {
            int allSum = 0;
            for (int number = 10000; number < 100000; number++)
                if (Sum(number) == number) allSum += number;
            Console.WriteLine(allSum);
            Console.WriteLine(Pow(-1, 2));
        }
        public static int Pow(int a, int b)
        {
            int k = a;
            if (b == 0) return 1;
            for (int i = 1; i < b; i++)
                a *= k;
            return a;   
        }
        public static int Sum(int number)
        {
            int sum = 0;
            for (int j = 10; number > 0; number /= 10)
                sum += Pow((number % j), 5);
            return sum;
        }
    }
}

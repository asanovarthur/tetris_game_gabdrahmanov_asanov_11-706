﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter3Task6
{
    class Program
    {
        static void Main()
        {
            int prenumber = int.Parse(Console.ReadLine());
            int number = int.Parse(Console.ReadLine());
            int nextnumber = int.Parse(Console.ReadLine());
            Console.WriteLine(CheckIncrease(prenumber, number, nextnumber));
        }
        public static bool CheckIncrease(int prenumber, int number, int nextnumber)
        {
            while (nextnumber != 0)
            {
                if ((number - prenumber) <= 0 || (nextnumber - number <= 0)) break;
                prenumber = number;
                number = nextnumber;
                nextnumber = int.Parse(Console.ReadLine());
            }
            if (nextnumber == 0) nextnumber = number + 1;
            return (number > prenumber && nextnumber > number);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter2Task11
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());
            int k = int.Parse(Console.ReadLine());
            Console.WriteLine(Check(ConvertToDecimal(number), k));
        }
        public static int ConvertToDecimal(int number)
        {
            int p = 1;
            int decimalNumber = 0;
            while (number > 0)
            {
                if (number % 10 == 1)
                {
                    decimalNumber += p;
                }
                p *= 2;
                number /= 10;
            }
            return decimalNumber;
        }
        public static bool Check (int a, int b)
        {
            return (a % b == 0);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter4Task7
{
    class Program
    {
        static void Main()
        {
            int k = int.Parse(Console.ReadLine());
            Console.WriteLine(LCM(k));
        }
        public static long LCM(int k)
        {
            long result = 1;
            for (int i = 2; i <= k; i++)
            {
                result = (result * i) / DCD(result, i);
            }
            return result;
        }
        public static long DCD(long a, long b)
        {
            while (a != b)
            {
                if (a > b) a = a - b;
                else b = b - a;
            }
            return a;
        }
    }
}

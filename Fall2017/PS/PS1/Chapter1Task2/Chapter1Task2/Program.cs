﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter1Task2
{
    class Program
    {
        static void Main()
        {
            int x1 = Console.Read();
            int y1 = Console.Read();
            Console.ReadLine();
            int x2 = Console.Read();
            int y2 = Console.Read();
            if (PawnsMovement(x1, y1, x2, y2)) Console.WriteLine("YES");
            else  Console.WriteLine("NO");
        }
        public static bool PawnsMovement (int x1, int y1, int x2, int y2)
        {
            return ((x2 - x1) == 0 && (y2 - y1) == 1) || ((x2 - x1) == 0 && (y2 - y1) == 2 && (y1 == '2'));
            //в условие это не сказано, но вот ход, когда пешка съедает фигугу 
            // || ((x2 - x1 == 1) && (y2 - y1 == 1))
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter1Task15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("eps: ");
            double eps = double.Parse(Console.ReadLine());
            Console.WriteLine(16 * GetArc(eps, (double) 1 / 5) - 4 * (GetArc(eps, (double) 1 / 239)));
        }
        static double GetArc(double eps, double x)
        {
            double exp = x;
            double item = x;
            int k = 1;
            while (Math.Abs(item) > eps)
            {
                item *= ((-1) * x * x * (2 * k - 1)) / (2 * k + 1);
                exp += item;
                k++;
            }
            return exp;
        }
    }
}

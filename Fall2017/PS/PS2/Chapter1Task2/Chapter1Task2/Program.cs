﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter1Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("eps: ");
            double eps = double.Parse(Console.ReadLine());
            Console.Write("x: ");
            double x = double.Parse(Console.ReadLine());
            Console.WriteLine(GetSum(eps, x));
        }
        public static double GetSum(double eps, double x)
        {
            double exp = 1;
            double item = 1;
            int k = 1;
            while (Math.Abs(item) > eps)
            {
                item *= (2 * k + 1) / (x * x * (2 * k - 1) * (2 * k) * (2 * (k - 1) + 1));
                exp += item;
                k++;
            }
            return exp;
        }
    }
}

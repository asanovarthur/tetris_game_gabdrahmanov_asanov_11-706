﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter3Task13
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("n: ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("b: ");
            double b = double.Parse(Console.ReadLine());
            Console.Write("a: ");
            double a = double.Parse(Console.ReadLine());
            Console.Write("Left Riemann sum: ");
            Console.WriteLine(GetLeftRiemannSum(n, b, a));
            Console.Write("Right Riemann sum: ");
            Console.WriteLine(GetRightRiemannSum(n, b, a));
            Console.Write("Trapezoidal rule sum: ");
            Console.WriteLine(GetTrapezoidalRuleSum(n, b, a));
            Console.Write("Simpson's rule sum: ");
            Console.WriteLine(GetSimpsonsRuleSum(n, b, a));
            Console.Write("Monte Carlo method: ");
            Console.WriteLine(GetMonteCarloMethodSum(n, b, a));
        }
        static double Func(double x)
        {
            return Math.Tan(Math.Sin(2 * x) * Math.Sin(2 * x));
        }
        public static double GetLeftRiemannSum(int n, double b, double a)
        {
            var sum = 0.0;
            var h = (b - a) / n;
            for (var x = a; x < b; x += h)
                sum += h * Func(x);
            return sum;
        }
        public static double GetRightRiemannSum(int n, double b, double a)
        {
            var sum = 0.0;
            var h = (b - a) / n;
            for (var x = a; x < b; x += h)
                sum += h * Func(x + h);
            return sum;
        }
        public static double GetTrapezoidalRuleSum(int n, double b, double a)
        {
            var sum = 0.0;
            var h = (b - a) / n;
            for (var x = a; x < b; x += h)
                sum += h * (Func(x + h) + Func(x)) / 2;
            return sum;
        }
        public static double GetSimpsonsRuleSum(int n, double b, double a)
        {
            var h = (b - a) / n;
            var x = a + h;
            var sum = 0.0;
            while (x < b)
            {
                sum += 4 * Func(x);
                x += h;
                sum += 2 * Func(x);
                x += h;
            }
            return (h / 3) * (sum + Func(a) - Func(b));
        }
        public static double GetMonteCarloMethodSum(int n, double b, double a)
        {
            var h = (b - a) / n;
            var sum = 0.0;
            for(var x = a; x <= b; x += h)
                sum += Func(x);
            return sum * (b - a) / n;
        }
    }
}

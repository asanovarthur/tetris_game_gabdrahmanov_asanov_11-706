﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter1Task15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("eps: ");
            double eps = double.Parse(Console.ReadLine());
            Console.Write("x: ");
            double x = double.Parse(Console.ReadLine());
            Console.Write("alpha: ");
            double alpha = double.Parse(Console.ReadLine());
            Console.WriteLine(GetSum(eps, x, alpha));
        }
        static double GetSum(double eps, double x, double aplha)
        {
            double exp = 1;
            double item = 1;
            int k = 1;
            while (Math.Abs(item) > eps)
            {
                item *= ((aplha - k + 1) * x) / k;
                exp += item;
                k++;
            }
            return exp;
        }
    }
}
